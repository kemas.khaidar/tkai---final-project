from django.shortcuts import render
from django.http import JsonResponse
import requests

def delete(request, id_pengguna, id_jadwal):
    query = queryBuilder(id_pengguna, id_jadwal)
    url = 'http://3.1.213.203:32172/query?q=' + query
    # urldummy = 'http://3.1.213.203:32172/query?q=SELECT%20*%20FROM%20jadwal'
    # r = requests.get(urldummy, params=request.GET)
    # print(r.json())
    r = requests.get(url, params=request.GET)
    return JsonResponse(r.json())

def queryBuilder(id_pengguna, id_jadwal):
	result = 'DELETE FROM jadwal WHERE pengguna_id=' + id_pengguna + ' AND id=' + id_jadwal
	return result.replace(' ', '%20')