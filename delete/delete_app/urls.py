from django.conf.urls import url
from delete_app import views

urlpatterns = [
    url(r'^(\d+)/(\d+)/$', views.delete, name='delete'),
]