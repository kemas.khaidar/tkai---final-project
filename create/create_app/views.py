from django.shortcuts import render
from django.http import JsonResponse
import requests
import os
from django.views.decorators.csrf import csrf_exempt

def create_get(request, id_pengguna, jam_mulai, jam_selesai, isi):
    query = queryBuilder(id_pengguna, jam_mulai, jam_selesai, isi)
    url = os.getenv("URL") + query
    r = requests.get(url, params=request.GET)
    return JsonResponse(r.json())

@csrf_exempt
def create_post(request):
    data = request.POST
    try:
        id_pengguna = data["id_pengguna"]
        jam_mulai = data["jam_mulai"]
        jam_selesai = data["jam_selesai"]
        isi = data["isi"]
    except:
        error = {
            "status" : "error",
            "message" : "jadwal data must be complete"
        }
        return JsonResponse(error)

    return create_get(request, id_pengguna, jam_mulai, jam_selesai, isi)

def queryBuilder(id_pengguna, jam_mulai, jam_selesai, isi):
    query = "INSERT INTO jadwal(pengguna_id, jam_mulai, jam_selesai, isi) VALUES "
    query += "({},\'{}\',\'{}\',\'{}\')".format(id_pengguna, jam_mulai, jam_selesai, isi)
    print(query)
    return query