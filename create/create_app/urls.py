from django.conf.urls import url
from create_app import views

urlpatterns = [
    url(r'^post/', views.create_post, name='create')
]