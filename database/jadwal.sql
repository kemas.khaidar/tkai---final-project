CREATE TABLE pengguna(
    id serial PRIMARY KEY,
    nama VARCHAR
);

CREATE TABLE jadwal(
   id serial PRIMARY KEY,
   jam_mulai TIMESTAMP,
   jam_selesai TIMESTAMP,
   isi VARCHAR,
   pengguna_id INTEGER REFERENCES pengguna(id) ON DELETE CASCADE
);