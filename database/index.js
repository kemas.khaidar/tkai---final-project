require('dotenv').config();
const express = require('express'); 
const app = express();
const { Pool } = require('pg');

const pool = new Pool({
    user: process.env.PG_USER,
    host: process.env.PG_HOST,
    database: process.env.PG_DB_NAME,
    password: process.env.PG_PASS,
    port: process.env.PG_PORT,
})

const port = 8000;

app.listen(port, () => console.log(`Hello world app listening on port ${port}!`));

app.get('/', (req, res) => {
    res.send('To query use /query?q="query string"');
});

app.get('/query', async (req, res) => {
    const query = req.query.q;
    try {
        const result = await pool.query(query);
        const response = {
            status: "success",
            count: result.rows.length,
            data: result.rows,
        }
        res.send(response);
    }
    catch(error) {
        const errorResponse = {
            status: "error",
            message: new String(error),
        }
        res.send(errorResponse);
    }
})