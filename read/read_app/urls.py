from django.conf.urls import url
from read_app import views

urlpatterns = [
    url(r'^(\d+)/$', views.read, name='read'),
]