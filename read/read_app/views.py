from django.shortcuts import render
from django.http import JsonResponse
import requests
import os

# Create your views here.
def read(request, id_pengguna):
    query = queryBuilder(id_pengguna)
    url = os.getenv('URL') + query
    r = requests.get(url, params=request.GET)
    return JsonResponse(r.json())

def queryBuilder(id_pengguna):
	result = 'SELECT * FROM jadwal WHERE pengguna_id=' + id_pengguna
	return result.replace(' ', '%20')