from django.shortcuts import render
from django.http import JsonResponse
import requests
import os
from django.views.decorators.csrf import csrf_exempt

def update_get(request, id_pengguna, id_jadwal, jam_mulai, jam_selesai, isi):
    query = queryBuilder(id_pengguna, id_jadwal, jam_mulai, jam_selesai, isi)
    url = os.getenv("URL") + query
    r = requests.get(url, params=request.GET)
    return JsonResponse(r.json())

@csrf_exempt
def update_post(request):
    data = request.POST

    id_pengguna = data["id_pengguna"]
    id_jadwal = data["id"]
    jam_mulai = data["jam_mulai"]
    jam_selesai = data["jam_selesai"]
    isi = data["isi"]

    return update_get(request, id_pengguna, id_jadwal, jam_mulai, jam_selesai, isi)

def queryBuilder(id_pengguna, id_jadwal, jam_mulai, jam_selesai, isi):
    result = "UPDATE jadwal SET "

    changes = []
    if(jam_mulai is not None and len(jam_mulai) > 0):
        changes.append("jam_mulai = '{}'".format(jam_mulai))
    if(jam_selesai is not None and len(jam_selesai) > 0):
        changes.append("jam_selesai = '{}'".format(jam_selesai))
    if(isi is not None and len(isi) > 0):
        changes.append("isi = '{}'".format(isi))
    result += ", ".join(changes)

    result += " WHERE id = {} AND pengguna_id = {};".format(id_jadwal, id_pengguna)

    result = result.replace(' ',' ')
    # print(result)
    return result

# testing langsung
# if __name__ == "__main__":
#     print(queryBuilder(1,15,"16.00","18.00","hehehe"))


# http://127.0.0.1:8000/update/1/3/now()/now()/hehe/