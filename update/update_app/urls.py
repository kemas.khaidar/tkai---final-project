from django.conf.urls import url
from update_app import views

urlpatterns = [
    url(r'^get/(\d*)/(\d*)/([\s\S]*)/([\s\S]*)/([\s\S]*)/$', views.update_get, name='update_get'),
    url(r'^post/$', views.update_post, name='update_post')
]

# 2019-12-03T04:46:58.657Z